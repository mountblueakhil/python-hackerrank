# Enter your code here. Read input from STDIN. Print output to STDOUT
from itertools import combinations

n=int(input())

let = input().split()
s= []
for j in range(n) :
    if let[j] == "a":
        s.append(j+1)   
k= int(input())
arr =[]
for i in range(n):
    arr.append(i+1)
    
op = list(combinations(arr,k))

count=0
for each in op :
    
    for lt in s :
        if lt in each :
            count = count +1
            break
    
print(count/len(op))