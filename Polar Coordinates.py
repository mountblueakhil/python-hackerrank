# Enter your code here. Read input from STDIN. Print output to STDOUT
import cmath

c= complex(input())

x,y = c.real,c.imag

result1 = abs(complex(float(x), (y)))
result2 = cmath.phase(complex(float(x), (y)))
 
print(result1)
print(result2)
 