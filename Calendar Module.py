# Enter your code here. Read input from STDIN. Print output to STDOUT
import calendar


a= input().split()
m = int(a[0])
d = int(a[1])
y = int(a[2])
c = calendar.weekday(y, m, d)

if c == 0:
    print("MONDAY")
elif c == 1:
    print("TUESDAY")
elif c == 2:
    print("WEDNESDAY")
elif c==3:
    print("THURSDAY")
elif c==4:
    print("FRIDAY")
elif c== 5:
    print("SATURDAY")
else :
    print("SUNDAY")