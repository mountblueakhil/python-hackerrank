# Enter your code here. Read input from STDIN. Print output to STDOUT
n = int(input())

items = {}
for i in range(n):
    
    op = input().split()
    
    
    number = int(op[-1])
    
    op.pop()
    if len(op)>1 :
        op = " ".join(op)
    else :
        op =op[0]
        
    if op in items :
        items[op] = items[op]+number
    else :
        items[op] = number
        

for i,l in items.items() :
    
    print(i,l)