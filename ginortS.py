# Enter your code here. Read input from STDIN. Print output to STDOUT

a= input()

lower = ""
upper = ""
even = ""
odd = ""
for i in a :
    
    if i.islower():
        lower=lower +i
    elif i.isdigit() :
        if int(i)%2 == 0 :
            even=even +i
        else :
            odd = odd +i
    else :
        upper = upper +i

lower = sorted(lower)
upper = sorted(upper)
even = sorted(even)
odd = sorted(odd)
arr =(lower+upper+odd+even)

print("".join(arr))