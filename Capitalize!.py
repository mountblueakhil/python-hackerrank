#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the solve function below.
def solve(s):
    op= s[0].upper() 
    
    s=op +s[1:]
    # print(s)
    l=len(s)
    for i in range(l):
        
        if s[i] == " ":
            op = s[i+1].upper() 
            
            s= s[:i+1]+op + s[i+2:]
    return s

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = solve(s)

    fptr.write(result + '\n')

    fptr.close()
