import numpy

n=int(input())
arr =[]
for i in range(n):
    a= list(map(float,input().split()))
    arr.append(a)
print(round(numpy.linalg.det(arr),2))  