cube = lambda x: x**3

def fibonacci(n):
    # return a list of fibonacci numbers
    prev, curr = 0,1
    arr =[]
    if n==0 :
        return []
    
    elif n == 1 :
        arr.append(0)
    else :
        arr.append(0)
        arr.append(1)
        for i in range(2,n):
            prev,curr = curr ,curr+prev
            arr.append(curr)
        
    return arr

    

if __name__ == '__main__':
    n = int(input())
    print(list(map(cube, fibonacci(n))))