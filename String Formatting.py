def print_formatted(number):
    # your code goes here
    
    # string1.rjust(10, ' ')
    l = len(str(bin(n)).replace("0b",""))
    for i in range(1,number+1):
        
        o = str(oct(i)).replace("0o","").rjust(l," ")
        h = str(hex(i)).replace("0x","").rjust(l," ")
        b = str(bin(i)).replace("0b","").rjust(l," ")
        s= str(i).rjust(l," ")
        print(s+" "+o+" "+h+" "+b)
        

if __name__ == '__main__':
    n = int(input())
    print_formatted(n)