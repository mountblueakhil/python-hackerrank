import numpy

def arrays(arr):
    # complete this function
    # use numpy.array
    arr = arr[::-1]
    op = numpy.array(arr,float)
    
    return op

arr = input().strip().split(' ')
result = arrays(arr)
print(result)