import operator

def person_lister(f):
    def inner(people):
        # complete the function
        op = map(f,sorted(people ,key = lambda x : int(x[-2])))
        # print(people)
        # inner =[]
        # for i in people:
        #     if people[-1] == "M" :
        #         op = "Mr. " +i[i]+" "+i[1]
        #         inner.append(op)
        #         print(op)
        #     else :
        #         op = "Ms. " +i[0]+" "+i[1]
        #         inner.append(op)
        #         print(op)
        return op
    return inner

@person_lister
def name_format(person):
    return ("Mr. " if person[3] == "M" else "Ms. ") + person[0] + " " + person[1]

if __name__ == '__main__':
    people = [input().split() for i in range(int(input()))]
    print(*name_format(people), sep='\n')