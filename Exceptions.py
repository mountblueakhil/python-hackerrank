# Enter your code here. Read input from STDIN. Print output to STDOUT


a=int(input())


for i in range(a) :
    
    try :
        
        op =list(map(int,input().split()))
       
        if (str(op[1]).isdigit()):
            if op[1] == 0 :
                raise ZeroDivisionError
            else :
                result = op[0]/op[1]
                print(int(result))
        else :
            raise ValueError
        
        
    except ValueError as v: 
        print("Error Code:",v)
    except ZeroDivisionError as z: 
        print("Error Code: integer division or modulo by zero")