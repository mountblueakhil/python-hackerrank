def count_substring(string, sub_string):
    l=len(sub_string)
    L=len(string)
    count = 0
    for i in range(L-l+1):
        if string[i:i+l] == sub_string:
            count=count+1
        
    return count

if __name__ == '__main__':
    string = input().strip()
    sub_string = input().strip()
    
    count = count_substring(string, sub_string)
    print(count)