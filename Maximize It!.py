# Enter your code here. Read input from STDIN. Print output to STDOUT
from itertools import product
k,m = list(map(int,input().split()))
arr =[]
for i in range(k):
    
    a= list(map(int,input().split()))
    arr.append(a[1:])

    
op = list(product(*arr))

res = 0
for j in op :
    count =0
    for e in j :
        count =count + (e**2)
    if (count % m) > res :
        res = count%m 
print(res)
