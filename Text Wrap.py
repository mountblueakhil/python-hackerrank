import textwrap

def wrap(string, max_width):
    s = ""
    for i in range(len(string)) :
        s=s+string[i]
        if (i+1)%max_width == 0 :
            print(s)
            s=""
    if s!= "" :
        return s
    else :
        return 

if __name__ == '__main__':
    string, max_width = input(), int(input())
    result = wrap(string, max_width)
    print(result)