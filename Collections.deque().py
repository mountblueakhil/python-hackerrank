# Enter your code here. Read input from STDIN. Print output to STDOUT
from collections import deque

d = deque()
 
n= int(input())
 
for i in range(n):
    
    a= input().split()
    
    if a[0]=="append" :
        d.append((a[1]))
    elif a[0]=="appendleft" :
        d.appendleft((a[1]))
    elif a[0]=="extend" :
        d.extend((a[1]))
    elif a[0]=="extendleft" :
        d.extendleft((a[1]))
    elif a[0]=="count" :
        d.count((a[1]))
    elif a[0]=="pop" :
        d.pop()
    elif a[0]=="popleft" :
        d.popleft()
    elif a[0]=="remove" :
        d.remove((a[1]))
    elif a[0]=="reverse" :
        d.remove()
    elif a[0]=="rotate" :
        d.remove((a[1]))
    elif a[0]=="print" :
        print(d)
       
res = ""
for i in d :
    res = res+  i +" "   
    
print(res)